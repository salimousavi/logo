# Logo

<h3> Installation </h3>

Download this module and add in module directory and enable it.

<h3> How to work? </h3>
This module create a vocabulary that named <code>logo</code>.<br>

<b>Fields:</b>
 <br><code> field_logo </code>

<b>Route request:</b>
 <br>
 <code> api/logo </code>

<b>Response:</b>
 <br><code> Source of logo </code> 