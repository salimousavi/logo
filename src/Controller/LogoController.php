<?php

namespace Drupal\logo\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\taxonomy\Entity\Term;

class LogoController {

    public function index() {

        $tids = \Drupal::entityQuery('taxonomy_term')
            ->condition('vid', "logo")
            ->sort('tid', 'DESC')
            ->range(0,1)
            ->execute();

        if ($tids) {
            return new JsonResponse(file_create_url(Term::load(reset($tids))->field_logo->entity->uri->value));
        }

        return new JsonResponse(null);
    }
}